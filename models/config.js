const knex = require("../db/db");

const createConfig = (serverId, itemId) => {
	return knex('Config').insert({Server_id: serverId, Item_id: itemId})
}

const setConfigEnabled = (id, enabled) => {
	return knex('Config').update({Enabled: enabled}).where({id: id})
};

module.exports = {
	createConfig,
	setConfigEnabled,
};
