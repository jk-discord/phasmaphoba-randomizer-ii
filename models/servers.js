const knex = require("../db/db");

const getServer = (serverId) => {
	return knex("Servers").select("*").where("Server_id", serverId);
};

const createServer = (serverId) => {
	return knex("Servers").insert({ Server_id: serverId });
};

module.exports = {
	getServer,
	createServer,
};
