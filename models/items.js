const knex = require("../db/db");

const getItems = () => {
	return knex("Items").select('id')
}

const getItemsV = (serverId) => {
	return knex("GetItems").select("*").where("Server_id", serverId);
};

const getItemVByName = (serverId, itemName) => {
	return knex("GetItems")
		.select("*")
		.where({ Server_id: serverId, Name: itemName })
		.first();
};

module.exports = {
	getItems,
	getItemsV,
	getItemVByName,
};
