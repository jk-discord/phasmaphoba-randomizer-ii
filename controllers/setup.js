const serverModel = require("../models/servers");
const itemsModel = require("../models/items");
const configModel = require("../models/config");

const setup = async (serverId) => {
	const server = await serverModel.getServer(serverId).catch((err) => {
		console.log(err);
	});

	if (server.length) return;

	const createdServerId = await serverModel
		.createServer(serverId)
		.catch((err) => {
			console.log(err);
		});

	const items = await itemsModel.getItems().catch((err) => {
		console.log(err);
	});

	if (!items.length) return;

	items.map(async (item) => {
		await configModel.createConfig(createdServerId, item.id);
	});
};

module.exports = {
	setup,
};
