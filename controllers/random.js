const randomView = require("../views/random");
const itemsModel = require("../models/items");

const getRandom = async (msg, args) => {
	if (isNaN(args[0]) && args.length) {
		return randomView.sendNotANumber(msg, args[0]);
	}

	let { lightSources, items } = await filterResults(msg.guild.id);

	let random = [];

	if (lightSources.length) {
		random.push(pickRandom(lightSources));
	}

	if (items.length) {
		let amount = args.length ? parseInt(args[0]) : 3;
		amount = amount > items.length ? items.length : amount;

		for (i = 0; i < amount; i++) {
			pickedItem = pickRandom(items);
			random.push(pickedItem);
			items = items.filter((e) => e !== pickedItem);
		}
	}

	randomView.sendRandom(msg, random);
};

const getTeam = async (msg, args) => {
	if (!args.length) {
		return randomView.sendNoArgs(msg);
	}

	let { lightSources, items } = await filterResults(msg.guild.id);

	let amount = isNaN(args[0]) ? 3 : args[0]
	if (!isNaN(args[0])) { args.shift(); }
	
	amount = amount > Math.floor(items.length / args.length) ? Math.floor(items.length / args.length) : amount;

	let random = []

	args.map((member) => {
		let randomMember = []
		if (lightSources.length) {
			randomMember.push(pickRandom(lightSources));
		}

		for (i = 0; i < amount; i++) {
			pickedItem = pickRandom(items);
			randomMember.push(pickedItem);
			items = items.filter((e) => e !== pickedItem);
		}

		random.push(randomMember)
	});

	randomView.sendTeam(msg, random, args)
};

const filterResults = async (serverId) => {
	const results = await itemsModel.getItemsV(serverId).catch((err) => {
		console.log(err);
	});

	const lightSources = results.filter(
		(result) => result.Type === "Light" && result.Enabled === 1
	);
	const items = results.filter(
		(result) => result.Type === "Item" && result.Enabled === 1
	);

	return {
		lightSources: lightSources,
		items: items,
	};
};

const pickRandom = (collection) => {
	return collection[Math.floor(Math.random() * collection.length)];
};

module.exports = {
	getRandom,
	getTeam,
};
