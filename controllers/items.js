const itemView = require("../views/items");
const itemModel = require("../models/items");
const configModel = require("../models/config");

const getItems = async (msg) => {
	const results = await itemModel.getItemsV(msg.guild.id).catch((err) => {
		console.log(err);
	});

	if (!results.length) {
		return
	}

	itemView.sendItems(msg, results);
};

const setItem = async (msg, args) => {
	if (!args.length) {
		return itemView.sendNoArgs(msg);
	}

	const itemName = args.join(" ");

	const item = await itemModel
		.getItemVByName(msg.guild.id, itemName)
		.catch((err) => {
			console.log(err);
		});

	if (!item) {
		return itemView.sendInvalidItem(msg, itemName);
	}

	const enabled = item.Enabled == 1 ? 0 : 1;

	await configModel.setConfigEnabled(item.id, enabled).catch((err) => {
		console.log(err);
	});

	itemView.sendUpdatedItem(msg, itemName, enabled);
};

module.exports = {
	getItems,
	setItem,
};
