const helpView = require("../views/help");

const getHelp = (msg, args, prefix) => {
	if(!args.length){
		return helpView.sendHelp(msg)
	}

	const command = args.join(" ")

	const commands = {
		ITEMS: "items",
		TOGGLE: "toggle",
		RANDOM: "random",
		TEAM: "team",
		HELP: "help",
	};
	
	switch(command){
		case commands.ITEMS:
			helpView.sendHelpItems(msg)
			break;
		case commands.TOGGLE:
			helpView.sendHelpToggle(msg)
			break;
		case commands.RANDOM:
			helpView.sendHelpRandom(msg)
			break;
		case commands.TEAM:
			helpView.sendHelpTeam(msg)
			break;
		default:
			helpView.sendInvalidArgs(msg, command, prefix)
			break;
	}
};

const getInvalid = (msg, prefix) => {
	helpView.sendInvalid(msg, prefix)
};

module.exports = {
	getHelp,
	getInvalid,
};
