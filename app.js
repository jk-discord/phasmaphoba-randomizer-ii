const Discord = require("discord.js");
const client = new Discord.Client();

const itemController = require("./controllers/items");
const randomizeController = require("./controllers/random");
const helpController = require("./controllers/help");
const setupController = require("./controllers/setup");

const BOT_PREFIX = "pr!";

client.on("ready", () => {
	console.log(`Logged in as ${client.user.tag}!`);
	client.user.setStatus("available");
	client.user.setActivity(`${BOT_PREFIX}help`, {
		type: "LISTENING",
	});
});

const COMMANDS = {
	ITEMS: "items",
	TOGGLE: "toggle",
	RANDOM: "random",
	TEAM: "team",
	HELP: "help",
};

client.on("message", async (msg) => {
	if (!msg.content.startsWith(BOT_PREFIX) || msg.author.bot) return;

	const args = msg.content.slice(BOT_PREFIX.length).trim().split(" ");
	const command = args.shift().toLowerCase();

	await setupController.setup(msg.guild.id);

	switch (command) {
		case COMMANDS.ITEMS:
			itemController.getItems(msg);
			break;
		case COMMANDS.TOGGLE:
			itemController.setItem(msg, args);
			break;
		case COMMANDS.RANDOM:
			randomizeController.getRandom(msg, args);
			break;
		case COMMANDS.TEAM:
			randomizeController.getTeam(msg, args);
			break;
		case COMMANDS.HELP:
			helpController.getHelp(msg, args, BOT_PREFIX);
			break;
		default:
			helpController.getInvalid(msg, BOT_PREFIX);
			break;
	}
});

client.login('Nzg0MzMyNzkyMTc4OTMzNzgx.X8nw1Q.xhJxL7zk8Xww8C0XlfwFeIFYvns');
