const Discord = require("discord.js");

const sendRandom = (msg, items) => {
	if (!items.length) {
		return msg.channel.send(`No items found to randomize...`);
	}

	let message = items.map((item) => {
		return `${item.Icon} - ${item.Name} \n`;
	});

	const embed = new Discord.MessageEmbed()
		.setColor("#EF6C00")
		.setTitle(`Items for ${msg.author.username}`)
		.setDescription(message)
		.setFooter(
			"⠀\nFor Candle and Smudge Sticks you may bring a Lighter \nFor Video Camera you may bring a Tripod"
		);

	msg.channel.send(embed);
};

const sendTeam = (msg, items, members) => {
	if (!items.length) {
		return msg.channel.send(`No items found to randomize...`);
	}

	members.map((member, i) => {
        let message = items[i].map((item) => {
            if(item){
                return `${item.Icon} - ${item.Name} \n`;
            }
        });

        const embed = new Discord.MessageEmbed()
		.setColor("#EF6C00")
		.setTitle(`Items for ${member}`)
		.setDescription(message)
		.setFooter(
			"⠀\nFor Candle and Smudge Sticks you may bring a Lighter \nFor Video Camera you may bring a Tripod"
		);

        msg.channel.send(embed);
    });
};

const sendNoArgs = (msg, args) => {
	msg.channel.send(`No arguments are given. Use \`pr!help team\` for more info`);
};

module.exports = {
	sendRandom,
	sendTeam,
	sendNoArgs,
};
