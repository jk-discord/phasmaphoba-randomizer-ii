const Discord = require("discord.js");

const sendHelp = (msg) => {

  const embed = new Discord.MessageEmbed()
    .setColor("#EF6C00")
    .setTitle(`Phasmaphobia Randomizer Help`)
    .setDescription(`My prefix is \`pr!\`. You can use this with the provided commands below.
    For more info about a command, use \`pr!help [command]\`.
    Want me on your Discord? You can use [this link...](https://discord.com/api/oauth2/authorize?client_id=784332792178933781&permissions=75840&scope=bot)
    ⠀`)
    .addFields(
      { name: 'Randomize', value: '`pr!random` `pr!team`\n⠀' },
      { name: 'Settings', value: '`pr!items` `pr!toggle`\n⠀' }
    )
    .setFooter(
      `This bot has been created by RipperJuice#6969`
    );

  msg.channel.send(embed)
}

const sendHelpItems = (msg) => {
  const embed = new Discord.MessageEmbed()
    .setColor("#EF6C00")
    .setTitle(`Items command`)
    .setDescription(`This command shows you the list of items which the randomizer will use.
  You can check if an item is enabled by it's mark.`)
    .addFields(
      { name: 'Usage', value: '`pr!items`' }
    );

  msg.channel.send(embed)
}

const sendHelpToggle = (msg) => {
  const embed = new Discord.MessageEmbed()
    .setColor("#EF6C00")
    .setTitle(`Toggle command`)
    .setDescription(`With this command you can enable or disable an item for the randomizer.
    If an item is disabled, it won't show up.`)
    .addFields(
      { name: 'Usage', value: '`pr!toggle [item name]`' },
      { name: 'Examples', value: '`pr!toggle Salt` `pr!toggle Strong Flashlight`' }
    );

  msg.channel.send(embed)
}

const sendHelpRandom = (msg) => {
  const embed = new Discord.MessageEmbed()
    .setColor("#EF6C00")
    .setTitle(`Random command`)
    .setDescription(`With this command you can start to randomize for yourself.
    By default you get a light source and 3 items. You can specify a number to get more/fewer items.`)
    .addFields(
      { name: 'Usage', value: '`pr!random [amount]`' },
      { name: 'Examples', value: '`pr!random` `pr!random 5`' }
    );

  msg.channel.send(embed)
}

const sendHelpTeam = (msg) => {
  const embed = new Discord.MessageEmbed()
    .setColor("#EF6C00")
    .setTitle(`Team command`)
    .setDescription(`With the team command you can get equipment for yourself and your crew.
    Everyone gets unique items, so nobody is useless during a hunt`)
    .addFields(
      { name: 'Usage', value: '`pr!team [amount] [team members]`' },
      { name: 'Examples', value: '`pr!team 3 John William` `pr!team Jane Bill Tom`' }
    );

  msg.channel.send(embed)
}

const sendInvalidArgs = (msg, command, prefix) => {
  msg.channel.send(
    `\`${prefix}help ${command}\` is not a valid command`
  );
}

const sendInvalid = (msg, prefix) => {
  msg.channel.send(
    `That's not a valid command :confused:\nUse \`${prefix}help\` to view available commands`
  );
}

module.exports = {
  sendHelp,
  sendHelpItems,
  sendHelpToggle,
  sendHelpRandom,
  sendHelpTeam,
  sendInvalidArgs,
  sendInvalid
}