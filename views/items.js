const sendItems = (msg, items) => {
	let message = "```\n";

	items.map((result) => {
		message = message + `${result.Enabled ? "✔️" : "❌"}\t${result.Name}\n`;
	});

	message = message + "```";

	msg.channel.send(message);
};

const sendUpdatedItem = (msg, itemName, enabled) => {
	msg.channel.send(`I've set \`${itemName}\` to \`${enabled ? "✔️" : "❌"}\``);
};

const sendNoArgs = (msg) => {
	msg.channel.send("No item is given");
};

const sendInvalidItem = (msg, itemName) => {
	msg.channel.send(`\`${itemName}\` is an invalid item. Remember that items are case sensitive`);
};


module.exports = {
	sendItems,
	sendUpdatedItem,
	sendNoArgs,
	sendInvalidItem,
};
