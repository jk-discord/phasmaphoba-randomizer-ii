const knex = require("knex");

const connectedDb = knex({
	client: "sqlite3",
	connection: {
		filename: "./db/store.db",
	},

	useNullAsDefault: true,
});

module.exports = connectedDb;
